/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/
// prompt
function inputInfo(){
	let fullname = prompt('Enter your full name: ');
	let age = prompt('Enter your age: ');
	let location = prompt('Enter your location: ');

	console.log('Hello ' + fullname);
	console.log('You are ' + age + ' years old.');
	console.log('You live in ' + location);
	alert('Thank you for this information')
}

inputInfo();
// end of prompt

// top 5 bands/music artists
console.log('My Top 5 Artists');
function displayFaveArtists(){
	let artist = ['Kendrick Lamar', 'J.Cole', 'Russ', 'Lupe Fiasco', 'Mac Miller'];

	for (let x = 0; x < artist.length; x++){
		console.log(x + 1 + '. ' + artist[x]);
	}
}

displayFaveArtists();
// end of top 5 bands/music artists

// top 5 movies
console.log('My Top 5 Movies')
function showTop5Movies(){
	let movie = [
	{film: 'The Godfather', rating: '97%'},
	{film: 'Forrest Gump', rating: '95%'},
	{film: 'Hacksaw Ridge', rating: '91%'},
	{film: 'The Count Of Monte Cristo', rating: '88%'},
	{film: 'The Wolf Of Wolf Street', rating: '83%'},
	
	]

	for (let x = 0; x < movie.length; x++) {
		console.log(x + 1 + '. ' + movie[x].film);
		console.log('Rotten Tomatoes Rating: ' + movie[x].rating);
	}
}

showTop5Movies();

let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();